from flask import Flask, render_template, Response
from generated import usage_pb2_grpc
import grpc
from client import usage_list_dates

app = Flask(__name__)
dates = ['2019-01-01 00:15:00', '2019-01-12 23:45:00']

@app.route('/')
def stream_get():
    return render_template('home.html')

@app.route('/_json')
def expose_json():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = usage_pb2_grpc.UsageStub(channel)
        usage_dict = usage_list_dates(stub, dates)
    return Response(usage_dict, mimetype='application/json')


if __name__ == '__main__':
    app.run()