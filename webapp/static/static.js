window.onload = function() {

var dataPoints = [];

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	title: {
		text: "Meter usage per 15 minutes"
	},
	axisY: {
		title: "Units of meter usage",
		titleFontSize: 24
	},
	data: [{
		type: "column",
        xValueFormatString:"DD-MM-YY hh:mm tt",
		yValueFormatString: "# Units",
		dataPoints: dataPoints
	}]
});

function addData(data) {
	for (var i = 0; i < data.length; i++) {
		dataPoints.push({
			x: new Date(data[i].datetime),
			y: data[i].energy
		});
	}
	chart.render();

}

$.getJSON("http://127.0.0.1:5000/_json", addData);

}