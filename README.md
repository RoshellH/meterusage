This repository contains my solution to the meterusage assignment. 
---

## Content of the repository

The repo consists of several files and folders. 

1. Server.py and client.py containing the gRPC server and client respectively.
2. Usage_resoures.py containing code to read the meterusage.csv file.
3. Generate_source.sh shows the code I used to generete the files of the "generated" folder.
4. Data folder containing the meterusage.csv.
5. Generated folder containing the generated code containing the classes for the messages and services
defined in the usage.proto file.
6. Protos folder containing the usage.proto file. In the usage.proto file the messages and services
that are used are defined. 
7. Webapp folder contains the small Flask app, HTML template and JavaScript file used to display the data. 

## How to use

To run the code first run server.py than client.py and app.py. To see the visualisation go to the
port the app is running on, usually localhost:5000. 

## What I did

You can think of many possible applications with this data. 
For this assignment I choose to explore two different applications. 

1. A scenario where you request the meterusage on a specific date. This is the most
basic example I could think of based on this data. Therefore it was a nice example to use
to get to know the gRPC framework. You could enter a date time in the front-end
and return the meter usage. 
2. Request the meterusage between two dates. Provide a window in which you want to know the
meter usage. This would allow for comparison of different periods of time or close inspection of
specific periods. 

I defined two services in the proto file. One that takes a specific date and returns the meterusage
on that date. The second one takes two dates and streams the meterusages between those dates. 
Both services are used in the client. If you run the client, the results will be printed. 

For the front-end I decided to use Flask to read the data in json format an display a graph. 
The graph shows the meterusage between two dates. For now I hard-coded the dates 
['2019-01-01 00:15:00', '2019-01-12 23:45:00']. You could image a scenario where the start and end
date are provided by the user. 

