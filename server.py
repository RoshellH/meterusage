from generated import usage_pb2_grpc, usage_pb2
from concurrent import futures
import grpc
import usage_resources


def get_usage_single_date(usage_db, dtt):

    """
    Returns the meter usage at a specific datetime in the database.
    :param usage_db: database generated in usage_recources.py containing the usage per 15 minutes
    :param dtt: The date time on which the get the meter usage
    :return: if the date time is in the data returns the specific row of the data.
    If not, return None. The row contains the date time and the meterusage.
    """

    for row in usage_db:
        if row.datetime == dtt:
            return row
    return None


class UsageServicer(usage_pb2_grpc.UsageServicer):
    """
    Provides methods that implement functionality of the meter usage server.
    Can be extended with additional functionality.
    """

    def __init__(self):
        """
        Initalized the UsageServicer class.
        """
        self.db = usage_resources.read_usage_database()

    def GetUsage(self, request, context):

        """
        Get the meter usage at a single point in time. If the date from request is not in the
        data, return an empty field for meterusage.
        :param request: datetime that is requested
        :param context: the grpc.UsageServicerContext object that provides RPC specific information
        :return: the meter usage at a single point in time.
        """

        feature = get_usage_single_date(self.db, request)
        if feature is None:
            return usage_pb2.MeterUsage(meterusage="", datetime=request)
        else:
            return feature

    def ListUsage(self, request, context):
        """
        Method streams meterusages between two dates.
        :param request: the datetimes of the date range.
        :param context: RPC specific context object
        :return: a stream of meter usages.
        """
        low = request.lo.date
        high = request.hi.date

        for feature in self.db:
            if (feature.datetime.date >= low) & (feature.datetime.date <= high):
                yield feature

def serve():
    """
    Start the server so the client can use the service.
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    usage_pb2_grpc.add_UsageServicer_to_server(UsageServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

serve()