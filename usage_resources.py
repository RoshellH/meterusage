from generated import usage_pb2
import pandas as pd

def read_usage_database():
    """
    Reads the meterusage csv file.
    :return: the content of the meterusage csv file as a sequence of usage_pb2.MeterUsage.
    """
    feature_list = []
    data = pd.read_csv('data/meterusage.csv')
    for row in range(data.shape[0]):
        feature = usage_pb2.MeterUsage(
            meterusage=data.iloc[row, 1],
            datetime=usage_pb2.DateTime(
                date=data.iloc[row, 0]))
        feature_list.append(feature)
    return feature_list