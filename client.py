from __future__ import print_function
import logging

import grpc
import pandas as pd
from generated import usage_pb2
from generated import usage_pb2_grpc

def usage_at_one_date(stub, date):
    """
    Method that
    :param stub: the stub generated from the UsageStub class.
    :param date: the datetime at which to get the meter usage
    :return: a list containing the datetime requested and the corresponding meterusage
    """
    feature = stub.GetUsage(date)
    if not feature.datetime:
        print("Server returned incomplete datetime")
        return

    if feature.meterusage:
        return [feature.datetime.date, feature.meterusage]
    else:
        print("Found no meter usage at %s" % feature.datetime)


def usage_get_feature(stub, dates):
    """
    Generates a dictionary with datetimes and meterusages for dates in the dates list.
    :param stub: the stub generated from the UsageStub class.
    :param dates: a list of dates on which to get the meterusage
    :return: a dictionary containing meter usages and datetime at different dates.
    """

    usage_list = [usage_at_one_date(stub, usage_pb2.DateTime(date=date)) for date in dates]
    usage_dict = {i[0]: i[1] for i in usage_list}
    return usage_dict


def usage_list_dates(stub, dates):
    """
    Generate a DataFrame
    :param stub:the stub generated from the UsageStub class.
    :param dates: a list containing the first and last datetime of the range we want to inspect
    :return: a json contaning the dates in the range and their corresponding meterusage.
    """

    date_range = usage_pb2.DateRange(
        lo=usage_pb2.DateTime(date=dates[0]),
        hi=usage_pb2.DateTime(date=dates[1]))

    features = stub.ListUsage(date_range)
    usage_list = [[feature.datetime.date, feature.meterusage] for feature in features]
    usage_json = pd.DataFrame(usage_list, columns=['datetime', 'energy']).to_json(orient='records')

    return usage_json


def run():
    """
    Runs the client. Dates are now hardcoded to demonstrate the code but could be given from the
    webapp or other frond-end.
    """

    with grpc.insecure_channel('localhost:50051') as channel:
        stub = usage_pb2_grpc.UsageStub(channel)
        dates = ['2019-01-01 00:45:00', '2019-01-02 05:15:00']
        print("-------------- GetFeature --------------")
        print(usage_get_feature(stub, dates))
        print("-------------- ListUsage --------------")
        print(usage_list_dates(stub, dates))

if __name__ == '__main__':
    logging.basicConfig()
    run()